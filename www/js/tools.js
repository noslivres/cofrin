// This is a JavaScript file

var textDiaSemana = "";
var textDataCompleta = "";


function dataInicio() {
  var semana = ["Domingo","Segunda-feira","Terça-feira","Quarta-feira","Quinta-feira","Sexta-Feira","Sábado"];
  var meses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
  var data = new Date(),
    dia = data.getDate(),
    mes = data.getMonth(),
    ano = data.getFullYear();
    hora = data.getHours(),
    minutos = data.getMinutes(),
    segundos = data.getSeconds(),
    dia_semana = semana[ data.getDay() ]; 
    
    textDiaSemana = dia_semana;
    textDataCompleta = dia + " " + meses[mes] + " " + ano;
    
}

function dataFormatada() {
  var data = new Date(),
    dia = data.getDate(),
    mes = data.getMonth(),
    ano = data.getFullYear();
    hora = data.getHours(),
    minutos = data.getMinutes(),
    segundos = data.getSeconds();
  return [ano,mes,dia,hora,minutos,segundos].join('');
}

function dataRegistro() {
  var data = new Date(),
    dia = data.getDate(),
    mes = parseInt(data.getMonth()) + 1,
    ano = data.getFullYear();
    result = ano + "-" + mes.toString() + "-" + dia;    
  return result;
}

function SelecionaData() {    
    $(function() {
     $( "#UltimaLimpeza1" ).datepicker();
  });

}


function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function genId() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g,
    function(c) {
      var r = Math.random() * 16 | 0,
        v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    }).toUpperCase();
}

function table() {

    $.getJSON('mares.json', function(data){

        len = data.mares.length;

        for (i = 0; i < len; i++){
            $("#dadosmare").append("<tr> <td>" + data.mares[i].Lua + "</td> <td>" + data.mares[i].Dia + "</td> <td>" + data.mares[i].Data + "</td> <td>" + data.mares[i].Hora + "</td> <td>" + data.mares[i].Alt + "</td></tr>");
        }
    });

}
